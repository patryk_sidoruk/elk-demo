package com.sonalake.elkdemo.lottery;

import org.springframework.stereotype.Service;

import java.util.Random;

@Service
class LotteryEngine {
  public static final int MIN_NUMBER = 1;
  public static final int MAX_NUMBER = 49;
  private static final int MAX_NUMBER_INTENTIONALLY_GENERATING_ERROR = 85;

  private final Random engine;

  LotteryEngine() {
    engine = new Random();
  }

  public int getLuckyNumber() {
    return engine.nextInt(MAX_NUMBER_INTENTIONALLY_GENERATING_ERROR - MIN_NUMBER + 1) + MIN_NUMBER;
  }
}