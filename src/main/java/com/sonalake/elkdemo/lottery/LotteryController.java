package com.sonalake.elkdemo.lottery;

import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("lottery")
@AllArgsConstructor
class LotteryController {
  private static final Logger logger = LoggerFactory.getLogger(LotteryController.class);
  @NonNull
  private final LotteryEngine lotteryEngine;

  @GetMapping("/play")
  ResponseEntity<Integer> findAll() {
    int luckyNumber = lotteryEngine.getLuckyNumber();
    if (luckyNumber > LotteryEngine.MAX_NUMBER) {
      String errorMessage = "server error 62259: number [" + luckyNumber + "] out of range";
      throw new IllegalStateException(errorMessage);
    }
    logger.info("your lucky number is " + luckyNumber);
    return ResponseEntity.status(HttpStatus.OK).body(luckyNumber);
  }

  @ExceptionHandler(IllegalStateException.class)
  ResponseEntity<String> handleConflict(IllegalStateException e) {
    return ResponseEntity.status(HttpStatus.CONFLICT).body(e.getMessage());
  }
}