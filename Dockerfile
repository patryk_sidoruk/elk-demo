FROM maven:3.6-jdk-11-slim

ADD target/elk-demo.jar app/elk-demo.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "app/elk-demo.jar"]